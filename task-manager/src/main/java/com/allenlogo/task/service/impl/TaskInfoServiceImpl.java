package com.allenlogo.task.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.allenlogo.task.service.TaskInfoService;
import com.allenlogo.task.vo.request.TIQutaAddReq;
import com.allenlogo.task.vo.request.TITimingAddReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/13
 * @Time 22:42
 * To change this template use File | Settings | File Templates.
 */
@Service("taskInfoService")
@Slf4j
public class TaskInfoServiceImpl implements TaskInfoService {
    @Override
    public void addTashInfo(TIQutaAddReq tiQutaAddReq) {
        log.info("{}", JSONObject.toJSONString(tiQutaAddReq));
    }

    @Override
    public void addTashInfo(TITimingAddReq tiTimingAddReqq) {
        log.info("{}", JSONObject.toJSONString(tiTimingAddReqq));
    }
}
